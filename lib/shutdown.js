'use strict';

const os = require('os');

// create the graceful shutdown middleware
function createMiddleware(server, options) {
  options = options || {};

  const logger = options.logger || console;

  // message to send to clients while service is shutting down
  const message = options.message || 'Service is stopping/restarting';

  // amount of time to wait for active connections before politely asking them to end
  const softTimeout = options.softTimeout || 5000;

  // amount of time to wait after soft timeout has expired before forcibly killing everything and shutting down
  const hardTimeout = options.hardTimeout || 5000;

  // signals to shut down is response to
  // keyed by signal name, value is actual signal number
  const signals = options.signals || {
    SIGHUP: os.SIGHUP,
    SIGINT: os.SIGINT,
    SIGTERM: os.SIGTERM
  };

  // the signal that actually ended us
  let terminalSignal = null;

  // changed to false when server is asked to shut down
  let running = true;

  // maintain a list of open connections
  const connections = [];
  server.on('connection', connection => {
    connections.push(connection);
    connection.on('close', () => {
      let i;
      while ((i = connections.indexOf(connection)) >= 0) {
        connections.splice(i, 1);
      }
      if (!running && connections.length <= 0) {
        // last dangling connection was closed
        finishShutdown();
      }
    });
  });

  // listen for signals
  Object.keys(signals).forEach(signal => {
    process.on(signal, () => {
      logger.log(`Received signal ${signal}...`);
      startShutdown(signal);
    });
  });

  // initial request to shutdown
  // stop accepting new connections
  // wait a bit if there are still open connections
  function startShutdown(signal) {
    running = false;
    terminalSignal = signal;
    if (connections.length > 0) {
      logger.log(`Waiting for ${connections.length} connection(s) to end...`);
      setTimeout(() => {
        if (connections.length > 0) {
          logger.log(`Ending ${connections.length} connection(s)...`);
          connections.forEach(connection => connection.end());
        }
        setTimeout(finishShutdown, hardTimeout);
      }, softTimeout);
    } else {
      finishShutdown();
    }
  }

  // finalize shutdown
  // if there are still any connections open, forcibly destroy them
  function finishShutdown() {
    if (connections.length > 0) {
      logger.log(`Destroying ${connections.length} connection(s) to close...`);
      connections.forEach(connection => connection.destroy());
    }
    server.close(() => {
      logger.log(`Shutdown due to ${terminalSignal}`);
      process.exit(128 + signals[terminalSignal]);
    });
  }

  // prevent new connections while server is shutting down
  function middleware(req, res, next) {
    if (running) {
      return next();
    }
    res.set('Connection', 'close');
    res.end(503, message);
  }

  return middleware;
}

module.exports = createMiddleware;
