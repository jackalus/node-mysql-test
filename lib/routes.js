'use strict';

const mysql = require('mysql');
const path = require('path');

/**
 * Define all routes and add them to the application.
 *
 * @todo Allow routes to be defined in separate files or this file will get too big
 * @param app The Express application to add routes to.
 */
function router(app) {
  /**
   * Endpoint to get the MySQL server version.
   */
  app.get('/mysql', (req, res) => {

    // create MySQL client
    let client;
    try {
      client = mysql.createConnection(process.env.MYSQL_URL || 'mysql://root@localhost/mysql');
    } catch (err) {
      sendError(res, err, 'Failed to get server info');
      return;
    }

    // connect to MySQL
    client.connect(err => {

      // handle connection error
      if (err) {
        sendError(res, err, 'Failed to get server info');
        return;
      }

      // query server version
      client.query('SELECT @@version', (err, results) => {

        // handle query error
        if (err) {
          sendError(res, err, 'Failed to get server info');
          return;
        }

        if (results && results[0] && results[0]['@@version']) {
          sendResponse(res, {version: results[0]['@@version']});
        } else {
          sendError(res, 'Unable to query @@version variable', 'Failed to get server info');
        }

      });
    });

  });

  /**
   * Endpoint to get the server version according to package.json
   */
  app.get('/version', (req, res) => {
    try {
      sendResponse(res, {version: require(path.resolve(process.env.SERVER_ROOT, 'package.json')).version});
    } catch (e) {
      sendError(res, e, 'Unable to determine server version');
    }
  });


}

/**
 * Send an error response to the client.
 *
 * @param res The response object.
 * @param err The error thrown (will be logged but not revealed to client.)
 * @param message The error message (will be sent to client.)
 */
function sendError(res, err, message) {
  res.writeHead(500, {'Content-Type': 'application/json'});
  res.end(JSON.stringify({'status': 500, 'error': message}));
  console.error(err);
}

/**
 * Send a success response to the client.
 *
 * @param res The response object.
 * @param object The object to send to the client as JSON.
 */
function sendResponse(res, object) {
  res.writeHead(200, {'Content-Type': 'application/json'});
  res.end(JSON.stringify(object));
  console.log(object);
}

module.exports = router;
