'use strict';

const dotenv = require('dotenv');
const express = require('express');
const http = require('http');
const morgan = require('morgan');
const shutdown = require('./lib/shutdown');

// read config from environment
dotenv.config();

// convenience to know where on the filesystem the server is located
process.env.SERVER_ROOT = __dirname;

const HTTP_ADDR = process.env.HTTP_ADDR || '0.0.0.0';
const HTTP_PORT = process.env.HTTP_PORT || 8080;

const app = express();
const server = http.createServer(app);

// request/response logging
app.use(morgan('tiny'));

// graceful shutdown
app.use(shutdown(server));

// routing
require('./lib/routes')(app);

// start server
server.listen(HTTP_PORT, HTTP_ADDR, () => {
  console.log(`Process ID ${process.pid} listening on ${HTTP_ADDR}:${HTTP_PORT}`);
});
